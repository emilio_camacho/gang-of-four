<?php

/**
 * Created by PhpStorm.
 * User: ecamacho
 * Date: 8/30/16
 * Time: 11:00 AM
 */
interface Person
{
    public function getName();

    public function getSurName();

    public function getSoons();
}

abstract class AbstractPerson implements Person
{
    protected $_name;
    protected $_surName;
    protected $_soons;

    public function getName()
    {
        return $this->_name;
    }
    public function getSurName()
    {
        return $this->_surName;
    }
}

class Father extends AbstractPerson
{
    public function __construct($name, $surName)
    {
        $this->_name = $name;
        $this->_surName = $surName;
        $this->_soons = "Calculate soons take a while";
    }

    public function getSoons()
    {
       return $this->_soons;
    }
}

class FatherProxy extends AbstractPerson
{
    protected $_realFather;
    public function __construct($name, $surName)
    {
        $this->_name = $name;
        $this->_surName = $surName;
    }
    protected function _lazyLoad()
    {
        if ($this->_realFather === null) {
            $this->_realFather = new father($this->_name, $this->_surName);
        }
    }

    public function getSoons()
    {
        $this->_lazyLoad();
        return $this->_realFather->getSoons();
    }
}

$father1 = new Father("Emilio", "Camacho");
$father2 = new FatherProxy("Emilio", "Camacho");
$father2->getSoons();
