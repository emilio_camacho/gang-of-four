<?php


class MailChamp
{
    public function sendEmailByMailChamp($message)
    {
        echo "Send the email by mailChamp: " . $message . "</br>";
    }
}

class Mandrill
{
    public function sendEmailByMandrill($message)
    {
        echo "Send email by mandrill: " . $message . "</br>";
    }
}

interface IMail
{
    public function send($message);
}

class MailChampAdapter implements IMail
{
    /**
     * @var MailChamp
     */
    private $mailClient;
    public function __construct(MailChamp $mailClient)
    {
        $this->mailClient = $mailClient;
    }

    public function send($message)
    {
         $this->mailClient->sendEmailByMailChamp($message);
    }
}

class MandrillAdapter implements IMail
{
    /**
     * @var Mandrill
     */
    private $mailClient;
    public function __construct(Mandrill $mailClient)
    {
        $this->mailClient = $mailClient;
    }

    public function send($message)
    {
         $this->mailClient->sendEmailByMandrill($message);
    }
}

class Test
{
    /**
     * @var IMail
     */
    private $mailClient;
    public function __construct(IMail $mailClient)
    {
        $this->mailClient = $mailClient;
    }

    public function sendMessage($message)
    {
        $this->mailClient->send($message);
    }
}

$testMandrill = new Test(new MandrillAdapter(new Mandrill()));
$testMailChamp = new Test(new MailChampAdapter(new MailChamp()));

$testMandrill->sendMessage("Message 1");
$testMailChamp->sendMessage("Message 2");
