<?php

interface DeviceInterface
{
    public function setPlatform(PlatformInterface $platform);
    public function watchMovie($movie);
}

abstract class Device implements DeviceInterface
{
    protected $platform;

    public function setPlatform(PlatformInterface $platform)
    {
        $this->platform = $platform;
    }
}

class PlayStation extends Device
{
    public function watchMovie($movie)
    {
        $movie .= " in a PlayStation";
        return $this->platform->displayMovie($movie);
    }
}

class PC extends Device
{
    public function watchMovie($movie)
    {
        $movie .= " with a PlayStation";
        return $this->platform->displayMovie($movie);
    }
}

class Iphone extends Device
{
    public function watchMovie($movie)
    {
        $movie .= " in an Iphone";
        return $this->platform->displayMovie($movie);
    }
}

interface PlatformInterface
{
    public function displayMovie($movie);
}

class Netflix implements PlatformInterface
{
    public function displayMovie($movie)
    {
        echo "Watching in Netflix " . $movie . "</br>";
    }
}

class Holo implements PlatformInterface
{
    public function displayMovie($movie)
    {
        echo "Watching in Holo " . $movie . "</br>";
    }
}

$play = new PlayStation();
$play->setPlatform(new Netflix());
$iphone = new Iphone();
$iphone->setPlatform(new Holo());

$play->watchMovie("The Lord of the rings");
$iphone->watchMovie("Matrix");