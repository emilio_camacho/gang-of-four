<?php

abstract class Thread
{
    protected $title;
    protected $description;

    public function __construct($title, $description)
    {
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getChilds()
    {
        return null;
    }

}

class ParentThread extends Thread
{
    /**
     * @var Thread[]
     */
    private $childs;

    /**
     * @return array
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * @param array $childs
     */
    public function setChilds(array $childs)
    {
        $this->childs = $childs;
    }

    public function addChild(Thread $child)
    {
        $this->childs[] = $child;
    }
}

class ChildThread extends Thread{}

class forum
{
    public static function display(Thread $thread, $level = 0)
    {
        echo implode('',array_fill(0,$level,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
        echo $thread->getTitle() . ": " . $thread->getDescription() . "</br>";
        if ($thread->getChilds()){
            foreach ($thread->getChilds() as $child) {
                self::display($child, $level+1);
            }
        }
    }
}

$main = new ParentThread("Pattern Designs in php", "Learn to implement pattern designs in php");
$creational = new ParentThread("Creational Designs", "Learn about creational Design");
$creational->addChild(new ChildThread("Abstract Factory","Pattern abstract factory"));
$creational->addChild(new ChildThread("Builder","Pattern Builder"));
$structural = new ParentThread("Structural Designs", "Learn about structural Design");
$structural->addChild(new ChildThread("Adapter", "Pattern Adapter"));
$structural->addChild(new ChildThread("Bridge", "Pattern Bridge"));
$main->addChild(new ChildThread("Created by", "Emilio"));
$main->addChild($creational);
$main->addChild($structural);

forum::display($main);
