<?php

abstract class Minion
{

    /**
     * @return mixed
     */
    public abstract function getEyes();

    /**
     * @return mixed
     */
    public function getColor()
    {
        return "yellow";
    }

    /**
     * @return mixed
     */
    public function getArms()
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function getLegs()
    {
        return 2;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return "unknown";
    }


}

class OneEye extends Minion
{
    public function getEyes()
    {
        return 1;
    }
}

class TwoEye extends Minion
{
    public function getEyes()
    {
        return 2;
    }
}

/**
 * Created by PhpStorm.
 * User: ecamacho
 * Date: 8/24/16
 * Time: 10:19 AM
 */
class CreateMinions
{
    const ONE_EYE = 'oneEye';
    const TWO_EYE = 'twoEye';


    private static $objects = [];

    private static $map = [
        self::ONE_EYE => 'OneEye',
        self::TWO_EYE => 'TwoEye'
    ];

    public static function getMinion($type)
    {
        if (!isset(self::$objects[$type])) {
            if (!isset(self::$map[$type])) {
                throw new Exception("That kind of minion does not exist");
            }
            self::$objects[$type] = new self::$map[$type]();
        }
        return self::$objects[$type];
    }
}
$minions = [
   CreateMinions::getMinion(CreateMinions::ONE_EYE),
   CreateMinions::getMinion(CreateMinions::ONE_EYE),
   CreateMinions::getMinion(CreateMinions::TWO_EYE),
   CreateMinions::getMinion(CreateMinions::ONE_EYE),
   CreateMinions::getMinion(CreateMinions::TWO_EYE),
   CreateMinions::getMinion(CreateMinions::ONE_EYE),
   CreateMinions::getMinion(CreateMinions::ONE_EYE),
   CreateMinions::getMinion(CreateMinions::ONE_EYE)
];

foreach ($minions as $key => $minion) {
    echo "Minion #$key has: " . $minion->getEyes() . " eye";
    if ($key > 0 && $minions[$key] == $minions[$key-1]) {
        echo " ,this is the same than minion #" . ($key - 1);
    }
    echo "</br>";
}