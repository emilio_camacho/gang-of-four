<?php

class User
{
    private $id;
    private $name;
    private $city;
    /**
     * @var Page
     */
    private $lastPageVisit;
    /**
     * @var \DateTime
     */
    private $lastTime;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return Page
     */
    public function getLastPageVisit()
    {
        return $this->lastPageVisit;
    }

    /**
     * @param Page $lastPageVisit
     */
    public function setLastPageVisit($lastPageVisit)
    {
        $this->lastPageVisit = $lastPageVisit;
    }

    /**
     * @return DateTime
     */
    public function getLastTime()
    {
        return $this->lastTime;
    }

    /**
     * @param DateTime $lastTime
     */
    public function setLastTime($lastTime)
    {
        $this->lastTime = $lastTime;
    }


}

class Page
{
    private $id;
    private $url;
    private $visits;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * @param mixed $visits
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;
    }


}

class Visit
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Page
     */
    private $page;
    /**
     * @var \DateTime
     */
    private $created_at;

    public function __construct(User $user, Page $page, \DateTime $date = null)
    {
        $this->user = $user;
        $this->page = $page;
        $this->created_at = $date ? : new \DateTime();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param Page $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param DateTime $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }


}


class VisitPageFacade
{
    private $user;

    private $page;

    public function __construct($userId, $pageId)
    {
        $this->user = $this->getUser($userId);
        $this->page = $this->getPage($pageId);
    }

    public function visit()
    {
        $visit = $this->createVisit();
        $this->persistVisit($visit);
        $this->incrementVisitInPage();
        $this->updateInfoUser();
    }

    private function getUser($userId)
    {
        return new User();
    }

    private function getPage($pageId)
    {
        return new Page();
    }

    private function createVisit()
    {
        echo "Create visit</br>";
        return new Visit($this->user, $this->page);
    }

    private function persistVisit(Visit $visit)
    {
        echo "Persist visit</br>";
    }

    private function incrementVisitInPage()
    {
        $this->page->setVisits($this->page->getVisits() + 1);
        echo "Increment visit in page</br>";
    }
    private function updateInfoUser()
    {
        echo "Update user info</br>";
        $this->user->setLastPageVisit($this->page);
        $this->user->setLastTime(new \DateTime());
    }

}

$visitFacade = new VisitPageFacade("Emilio", "Index");
$visitFacade->visit();