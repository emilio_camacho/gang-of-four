<?php

interface ICake
{
    public function describeCake();
}

class BaseCake implements ICake
{
    public function describeCake()
    {
        echo "biscuit and cream cake</br>";
    }
}

abstract class CakeDecorator implements ICake
{
    protected $cake;

    public function __construct(ICake $cake)
    {
        $this->cake = $cake;
    }
}

class ChocolatCake extends CakeDecorator
{
    public function describeCake()
    {
        echo "chocolat and ";
        $this->cake->describeCake();
    }
}

class FondantCake extends CakeDecorator
{
    public function describeCake()
    {
        echo "fondant and ";
        $this->cake->describeCake();
    }
}

$cake = new BaseCake();
$cake->describeCake();

$cake = new ChocolatCake($cake);
$cake->describeCake();

$cake = new FondantCake($cake);
$cake->describeCake();