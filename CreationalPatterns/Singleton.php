<?php
/**
 * Created by PhpStorm.
 * User: ecamacho
 * Date: 8/18/16
 */

class Me {

    private $eyeColor;
    private $hairColor;
    private $age;
    private static $instance;

    public static function createMe()
    {
        if (!self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    protected function __construct(){}
    private function __clone(){}
    private function __wakeup(){}

    /**
     * @return mixed
     */
    public function getEyeColor()
    {
        return $this->eyeColor;
    }

    /**
     * @param mixed $eyeColor
     */
    public function setEyeColor($eyeColor)
    {
        $this->eyeColor = $eyeColor;
    }

    /**
     * @return mixed
     */
    public function getHairColor()
    {
        return $this->hairColor;
    }

    /**
     * @param mixed $hairColor
     */
    public function setHairColor($hairColor)
    {
        $this->hairColor = $hairColor;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }


}

$babyEmilio = Me::createMe();
$babyEmilio->setEyeColor("Brown");
$babyEmilio->setHairColor("dark haired");
$babyEmilio->setAge(1);
var_dump($babyEmilio);

$adultEmilio = Me::createMe();
$adultEmilio->setAge(31);
var_dump($adultEmilio);
