<?php

/**
 * Created by PhpStorm.
 * User: ecamacho
 * Date: 8/18/16
 */
class GodDirector
{
    private $beingBuilder;

    public function __construct(AbstractBeingBuilder $beingBuilder)
    {
        $this->beingBuilder = $beingBuilder;
    }

    public function createBeign()
    {
        $this->beingBuilder->addEyes();
        $this->beingBuilder->addLegs();
        $this->beingBuilder->addArms();
    }

    public function getBeign()
    {
        return $this->beingBuilder->getBeing();
    }
}

abstract class AbstractBeingBuilder
{
    abstract public function addEyes();
    abstract public function addLegs();
    abstract public function addArms();
    abstract public function getBeing();
}

class SpiderBuilder extends AbstractBeingBuilder
{
    private $being;

    public function __construct()
    {
        $this->being = new Being();
    }

    public function addEyes()
    {
        $this->being->setEyes(6);
    }

    public function addLegs()
    {
        $this->being->setLegs(8);
    }

    public function addArms()
    {
        $this->being->setArms(0);
    }

    public function getBeing()
    {
        return $this->being;
    }
}

class HumanBuilder extends AbstractBeingBuilder
{
    private $being;

    public function __construct()
    {
        $this->being = new Being();
    }

    public function addEyes()
    {
        $this->being->setEyes(2);
    }

    public function addLegs()
    {
        $this->being->setLegs(2);
    }

    public function addArms()
    {
        $this->being->setArms(2);
    }

    public function getBeing()
    {
        return $this->being;
    }
}

class Being
{
    /**
     * @var Int
     */
    private $eyes;
    /**
     * @var Int
     */
    private $legs;
    /**
     * @var Int
     */
    private $arms;

    /**
     * @return Int
     */
    public function getEyes()
    {
        return $this->eyes;
    }

    /**
     * @param Int $eyes
     */
    public function setEyes($eyes)
    {
        $this->eyes = $eyes;
    }

    /**
     * @return Int
     */
    public function getLegs()
    {
        return $this->legs;
    }

    /**
     * @param Int $legs
     */
    public function setLegs($legs)
    {
        $this->legs = $legs;
    }

    /**
     * @return Int
     */
    public function getArms()
    {
        return $this->arms;
    }

    /**
     * @param Int $arms
     */
    public function setArms($arms)
    {
        $this->arms = $arms;
    }

}

$humanBuilder = new HumanBuilder();
$god = new GodDirector($humanBuilder);
$god->createBeign();
var_dump($god->getBeign());

$spiderBuilder = new SpiderBuilder();
$god = new GodDirector($spiderBuilder);
$god->createBeign($spiderBuilder);
var_dump($god->getBeign());

