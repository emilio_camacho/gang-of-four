<?php

/**
 * Created by PhpStorm.
 * User: ecamacho
 * Date: 8/17/16
 */
abstract class AbstractDessertFactroy
{
    /**
     * @param Float $price
     * @return AbstractCholatDessert
     */
    abstract public function makeChocolatDessert($price);

    /**
     * @param Float $price
     * @return AbstractVanillaDessert
     */
    abstract public function makeVanillaDessert($price);
}

class MilkShakeFactory extends AbstractDessertFactroy
{
    public function makeChocolatDessert($price)
    {
        return new MilkShakeChocolatDessert($price);
    }
    public function makeVanillaDessert($price)
    {
        return new MilkShakeVanillaDessert($price);
    }
}

class IceCreamFactory extends AbstractDessertFactroy
{
    public function makeChocolatDessert($price)
    {
        return new IceCreamChocolatDessert($price);
    }
    public function makeVanillaDessert($price)
    {
        return new IceCreamVanillaDessert($price);
    }
}



abstract class AbstractDessert
{
    abstract public function getPrice();
    abstract public function getDescription();
}

abstract class AbstractCholatDessert extends AbstractDessert{}

class MilkShakeChocolatDessert extends AbstractCholatDessert
{
    private $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function getDescription()
    {
        return 'Chocolat milkshake';
    }
}

class IceCreamChocolatDessert extends AbstractCholatDessert
{
    private $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function getDescription()
    {
        return "Chocolat icecream";
    }
}

abstract class AbstractVanillaDessert extends AbstractDessert{}

class MilkShakeVanillaDessert extends AbstractCholatDessert
{
    private $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function getDescription()
    {
        return "Vanilla milkshake";
    }
}

class IceCreamVanillaDessert extends AbstractCholatDessert
{
    private $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function getDescription()
    {
        return "Vanilla icecream";
    }
}

class CreateProducts
{
    private $factory;

    /**
     * @var AbstractDessert
     */
    private $products;

    public function __construct(AbstractDessertFactroy $factory)
    {
        $this->factory = $factory;
    }

    public function addChocolatDissert($price)
    {
        $this->products[] = $this->factory->makeChocolatDessert($price);
    }

    public function addVanillaDissert($price)
    {
        $this->products[] = $this->factory->makeVanillaDessert($price);
    }

    public function printProducts()
    {
        if (!empty($this->products)) {
            foreach ($this->products as $product) {
                echo $product->getDescription() . ": ".$product->getPrice()." € <br>";
            }
        } else {
            echo "Products is empty";
        }
    }
}


$milkshakeFactory = new MilkShakeFactory();
$icecreamFactory = new IceCreamFactory();

$createMilkShackes = new CreateProducts($milkshakeFactory);
$createMilkShackes->addChocolatDissert(3);
$createMilkShackes->addVanillaDissert(3);
$createMilkShackes->printProducts();

$createIcecreams = new CreateProducts($icecreamFactory);
$createIcecreams->addChocolatDissert(4);
$createIcecreams->addVanillaDissert(4);
$createIcecreams->printProducts();

