<?php

/**
 * Created by PhpStorm.
 * User: ecamacho
 * Date: 8/18/16
 */

class MobilPhone
{
    /**
     * @var string
     */
    private $brand;
    /**
     * @var string
     */
    private $model;
    /**
     * @var float
     */
    private $price;
    /**
     * @var \DateTime
     */
    private $create_at;

    public function __construct($brand, $model, $price, $create_at)
    {
        $this->brand = $brand;
        $this->model = $model;
        $this->price = $price;
        $this->create_at = $create_at;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
    }

}

class MobilPhoneFactory
{
    public static function create($brand, $model, $price)
    {
        return new MobilPhone($brand, $model, $price, new \DateTime());
    }
}

$iphone6 = MobilPhoneFactory::create("Iphone","6",695.99);
$galaxyS7Edge = MobilPhoneFactory::create("Samgsun","7Edge",600);

var_dump($iphone6);
var_dump($galaxyS7Edge);
