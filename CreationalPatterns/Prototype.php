<?php

/**
 * Created by PhpStorm.
 * User: ecamacho
 * Date: 8/18/16
 */
class City
{
    /**
     * @var String
     */
    private $oficialLanguage;

    /**
     * @var String
     */
    private $country;

    /**
     * @var String
     */
    private $name;

    public function __construct($name, $country, $oficialLanguage)
    {
        $this->name = $name;
        $this->country = $country;
        $this->oficialLanguage = $oficialLanguage;
    }

    /**
     * @return mixed
     */
    public function getOficialLanguage()
    {
        return $this->oficialLanguage;
    }

    /**
     * @param mixed $oficialLanguage
     */
    public function setOficialLanguage($oficialLanguage)
    {
        $this->oficialLanguage = $oficialLanguage;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}

$malaga = new City("Malaga", "Spain", "Spanish");
$cadiz = clone $malaga;
$cadiz->setName("Cadiz");
var_dump($malaga);
var_dump($cadiz);