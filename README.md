My implementation of pattern design from GOF (Gank of four) in php.

By now AbstractFactory, Builder, Factory, Prototype, Singleton,
Adapter, Bridge, Composite, Decorator, Facade, Flyweight and Proxy.